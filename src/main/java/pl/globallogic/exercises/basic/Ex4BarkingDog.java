package pl.globallogic.exercises.basic;

public class Ex4BarkingDog {

    public static boolean shouldWakeUp(boolean isBarking,int hourOfDay){
        if(isBarking){
            if(hourOfDay < 8 && hourOfDay>=0 || hourOfDay > 22){
                return true;
            }
        }
        return false;
    }
    public static void main(String[] args) {
        System.out.println(shouldWakeUp(true,7));
        System.out.println(shouldWakeUp(false,4));
        System.out.println(shouldWakeUp(true,0));
        System.out.println(shouldWakeUp(true,-10));

    }
}
