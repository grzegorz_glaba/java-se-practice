package pl.globallogic.exercises.basic;

public class Ex3MegaBytesConverter {

    public static void printMegaBytesAndKiloBytes(int kiloBytes){
        if(kiloBytes < 0)
        {
            System.out.println("Invalid parameter!");
        }else{
        System.out.println(kiloBytes + " KB = " + kiloBytes/1024 + " MB " + kiloBytes%1024 + " KB");
        }
    }

    public static void main(String[] args) {

        printMegaBytesAndKiloBytes(2500);
        printMegaBytesAndKiloBytes(-10000);

    }
}
