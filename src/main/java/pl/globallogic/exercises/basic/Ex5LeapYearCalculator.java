package pl.globallogic.exercises.basic;

public class Ex5LeapYearCalculator {

    public static boolean isLeapYear(int year) {
        if (year < 1 || year > 9999) {
            System.out.println("Invalid parameter!");
            return false;
        }
        if (year % 4 == 0) {
            if(year%100 == 0){
                return year % 400 == 0;
            }
            else{
                return true;
            }
        }
        return false;
    }
    public static void main(String[] args) {

        int notLeapYears[] = new int[]{1700,1800,1900,2100,2200,2300,2500,2600};

        for(int year : notLeapYears){
            System.out.println(isLeapYear(year));
        }
        System.out.println(isLeapYear(1600));
        System.out.println(isLeapYear(2000));
        System.out.println(isLeapYear(2400));
        System.out.println(isLeapYear(-1990));

    }
}
