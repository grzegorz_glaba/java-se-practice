package pl.globallogic.exercises.basic;

public class Ex10MinutesToYearsAndDaysCalculator {

    public static void printYearsAndDays(long minutes){
        if(minutes < 0){
            System.out.println("Invalid parameter!");
        }
        long minutesInYear = 525600;
        long years =minutes/minutesInYear;
        long days = (minutes - years*minutesInYear)/1440;
        if(days >= 365){
            years++;
            days = days - 365;
        }

        System.out.println(minutes + "min = " + years  + " y and "+ days +" d" );
    }
    public static void main(String[] args) {

        printYearsAndDays(525600);
        printYearsAndDays(1051200);
        printYearsAndDays(561600);
    }
}
