package pl.globallogic.exercises.basic;

public class Ex2SpeedConverter {

    private static long toMilesPerHour(double kilometersPerHour){
        if(kilometersPerHour < 0)
        {
            return -1;
        }
        return Math.round(kilometersPerHour*0.62137);

    }

    public static void printConversion(double kilometersPerHour){
        if(kilometersPerHour < 0) {
            System.out.println("Invalid Parameter!");
        }else {
            System.out.println(kilometersPerHour + " km/h = " + toMilesPerHour(kilometersPerHour) + " mil/h");
        }
    }

    public static void main(String[] args) {
        printConversion(1.5);
        printConversion(-10);
        printConversion(75.114);
    }
}
