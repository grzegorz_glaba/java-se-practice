package pl.globallogic.exercises.basic;

public class Ex8TeenNumberChecker {

    public static boolean isTeen(int num){
        if(num >= 13 && num <= 19){
            return true;
        }
        return false;
    }


    public static boolean hasTeen(int num1,int num2,int num3){
        int[] numArray = new int[]{num1,num2,num3};

        for(int num : numArray){
/*            if(num >= 13 && num <= 19){
                return true;
            }*/ //I can simply use isTeen to avoid code repetition

            if(isTeen(num)){
                return true;
            }

        }
        return false;
    }

    public static void main(String[] args) {
        System.out.println(hasTeen(9,99,19));
        System.out.println(hasTeen(23,15,42));
        System.out.println(hasTeen(22,23,34));

        System.out.println(isTeen(13));
        System.out.println(isTeen(20));

    }
}
