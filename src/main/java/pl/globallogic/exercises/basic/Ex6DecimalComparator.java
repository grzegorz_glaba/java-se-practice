package pl.globallogic.exercises.basic;

public class Ex6DecimalComparator {


    public static boolean areEqualByThreeDecimalPlaces(double num1,double num2){
        if(num1 > 0 && num2 > 0){
            return Math.floor(num1*1000) == Math.floor(num2*1000);
        }
        else if(num1 < 0 && num2 < 0){
            return Math.ceil(num1*1000) == Math.ceil(num2*1000);
        }
        return false;
    }

    public static void main(String[] args) {
        System.out.println(areEqualByThreeDecimalPlaces(-3.1756, -3.175));
        System.out.println(areEqualByThreeDecimalPlaces(3.175, 3.176));
        System.out.println(areEqualByThreeDecimalPlaces(3.0,3.0));
        System.out.println(areEqualByThreeDecimalPlaces(-3.123,3.123));
    }
}
