package pl.globallogic.exercises.basic;

public class Ex1PositiveNegativeOrZero {


    public static void checkNumber(int number)
    {
        if(number > 0){
            System.out.println("Positive");
        } else if(number < 0){
            System.out.println("Negative");
        } else{
            System.out.println("Zero");
        }
    }

    public static void main(String[] args) {
        checkNumber(10);
        checkNumber(-12);
        checkNumber(0);
    }
}
