package pl.globallogic.exercises.intermediate;

import java.util.ArrayList;
import java.util.List;

public class Player implements ISaveable{
    String name;
    String weapon;
    int hitPoints;
    int strength;

    public Player(String name,int hitPoints,int strength){
        this.name = name;
        this.hitPoints = hitPoints;
        this.strength = strength;
        this.weapon = "Sword";
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWeapon() {
        return weapon;
    }

    public void setWeapon(String weapon) {
        this.weapon = weapon;
    }

    public int getHitPoints() {
        return hitPoints;
    }

    public void setHitPoints(int hitPoints) {
        this.hitPoints = hitPoints;
    }

    public int getStrenght() {
        return strength;
    }

    public void setStrenght(int strenght) {
        this.strength = strenght;
    }
    public List<String> write(){
        List<String> fields = new ArrayList<String>();
        fields.add(this.name);
        fields.add(String.valueOf(this.hitPoints));
        fields.add(String.valueOf(this.strength));
        fields.add(this.weapon);
        return fields;
    }

    public void read(List<String> list){
        if (list != null && list.size() == 4) {
            this.name = list.get(0);
            this.hitPoints = Integer.parseInt(list.get(1));
            this.strength = Integer.parseInt(list.get(2));
            this.weapon = list.get(3);
        } else {
            System.out.println("Invalid saved values.");
        }
    }

    public String toString(){
        return "Player{name=" + this.name + ", hitPoints=" + this.hitPoints + ", strength=" + this.strength + ", weapon=" + this.weapon;
    }

}
