/* for ex46
package pl.globallogic.exercises.intermediate;

import java.util.ArrayList;
import java.util.LinkedList;

public class Album {
    private String name;
    private String artist;
    private ArrayList<Song> songs;

    public Album(String nameOfTheAlbum,String artist){
        this.name = nameOfTheAlbum;
        this.artist = artist;
        this.songs = new ArrayList<>();
    }

    public boolean addSong(String title,double duration){
        if(findSong(title) == null){
            Song newSong = new Song(title,duration);
            songs.add(newSong);
            return true;
        }
        return false;
    }

    public Song findSong(String title){
        for(Song song: songs){
            if(song.getTitle().equals(title)){
                return song;
            }
        }
        return null;

    }

    public boolean addToPlayList(int trackNumber,LinkedList<Song> playlist){
        if(trackNumber >= 1 && trackNumber <= songs.size()){
            if(findSong(songs.get(trackNumber - 1).getTitle()) != null) {
                Song newSong = songs.get(trackNumber - 1);
                playlist.add(newSong);
                return true;
            }
        }
        return false;
    }

    public boolean addToPlayList(String title, LinkedList<Song> playlist){
        if(findSong(title) != null){
            playlist.add(findSong(title));
            return true;
        }
        return false;
    }


}
*/
//for ex49
package pl.globallogic.exercises.intermediate;

import java.util.ArrayList;
import java.util.LinkedList;

public class Album {
    private String name;
    private String artist;
    private SongList songs;

    public Album(String nameOfTheAlbum,String artist){
        this.name = nameOfTheAlbum;
        this.artist = artist;
        this.songs = new SongList();
    }

    public boolean addSong(String title,double duration){
        if(findSong(title) == null){
            Song newSong = new Song(title,duration);
            songs.add(newSong);
            return true;
        }
        return false;
    }

    public Song findSong(String title){
        return songs.findSong(title);
    }
    public boolean addToPlayList(int trackNumber, LinkedList<Song> playlist) {
        Song songToAdd = songs.findSong(trackNumber);
        if (songToAdd != null) {
            playlist.add(songToAdd);
            return true;
        }
        return false;
    }

    public boolean addToPlayList(String title, LinkedList<Song> playlist) {
        Song foundSong = songs.findSong(title);
        if (foundSong != null) {
            playlist.add(foundSong);
            return true;
        }
        return false;
    }

    public static class SongList{

        private ArrayList<Song> songs;
        private SongList(){
            this.songs = new ArrayList<Song>();
        }

        private boolean add(Song song){
            if(findSong(song.getTitle()) != null){
                return false;
            }else{
                this.songs.add(song);
                return true;
            }
        }


        private Song findSong(int trackNumber){
            if(trackNumber >= 0 && trackNumber <= songs.size()){
                return songs.get(trackNumber -1);
            }
            return null;
        }
        private Song findSong(String title){
            for(Song song : songs){
                if(song.getTitle().equals(title)){
                    return song;
                }
            }
            return null;
        }
    }


}
