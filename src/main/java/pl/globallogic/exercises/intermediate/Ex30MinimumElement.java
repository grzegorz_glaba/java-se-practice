package pl.globallogic.exercises.intermediate;

import java.util.Scanner;

public class Ex30MinimumElement {

    private static int readInteger(){
        Scanner scanner = new Scanner(System.in);
        return scanner.nextInt();
    }
    private static int[] readElements(int sizeOfArray){
        Scanner scanner = new Scanner(System.in);
        int[] numbersArray = new int[sizeOfArray];
        for(int i = 0; i < sizeOfArray; i++){
            System.out.println("Element "+ i);
            numbersArray[i] = scanner.nextInt();
        }
        return numbersArray;
    }

    private static int findMin(int[] numbersArray){
        int minValue = Integer.MAX_VALUE;
        for(int i = 0; i < numbersArray.length;i++){
            if(numbersArray[i] < minValue){
                minValue = numbersArray[i];
            }
        }
        return minValue;
    }
    public static void main(String[] args) {
        System.out.println("Wprowadz rozmiar listy");
        int arraySize = readInteger();
        System.out.println("Wprowadz elementy listy ");
        int[] numbersArray = readElements(arraySize);
        System.out.println("Minimal value = " + findMin(numbersArray));

    }
}
