package pl.globallogic.exercises.intermediate;

public class Hamburger {
    private String name;
    private String meat;
    private String breadRollType;
    private double price;

   protected String addition1Name;
   protected String addition2Name;
   protected String addition3Name;
   protected String addition4Name;
   protected double addition1Price = 0.0;
   protected double addition2Price = 0.0;
   protected double addition3Price = 0.0;
   protected double addition4Price = 0.0;

    public Hamburger(String name, String meat, double price,String breadRollType) {
        this.name = name;
        this.meat = meat;
        this.breadRollType = breadRollType;
        this.price = price;
        System.out.println(name + "on a " + breadRollType + " with " + meat + ", price is " + price);
    }

    public void addHamburgerAddition1(String addition1Name,double addition1Price){
        this.addition1Name = addition1Name;
        this.addition1Price = addition1Price;
        System.out.println("Added " + addition1Name + " for " + addition1Price);
    }


    public void addHamburgerAddition2(String addition2Name,double addition2Price){
        this.addition1Name = addition2Name;
        this.addition1Price = addition2Price;
        System.out.println("Added " + addition2Name + " for " + addition2Price);

    }

    public void addHamburgerAddition3(String addition3Name,double addition3Price){
        this.addition3Name = addition3Name;
        this.addition3Price = addition3Price;
        System.out.println("Added " + addition3Name + " for " + addition3Price);

    }

    public void addHamburgerAddition4(String addition4Name,double addition4Price){
        this.addition4Name = addition4Name;
        this.addition4Price = addition4Price;
        System.out.println("Added " + addition4Name + " for " + addition4Price);

    }

    public double itemizeHamburger(){
        return this.price+this.addition1Price+this.addition2Price+this.addition3Price+this.addition4Price;
    }



}
