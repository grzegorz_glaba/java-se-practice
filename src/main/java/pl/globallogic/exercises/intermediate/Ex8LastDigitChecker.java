package pl.globallogic.exercises.intermediate;

import java.util.ArrayList;

public class Ex8LastDigitChecker {
    public static void main(String[] args) {
        System.out.println(hasSameLastDigit (41, 22, 71) == true);
        System.out.println(hasSameLastDigit (23, 32, 42) == true);
        System.out.println(hasSameLastDigit (99, 9, 999) == false);

    }

    private static boolean isValid(int number){
        if(number < 10 || number > 1000) return false;
        return true;
    }
    private static boolean hasSameLastDigit(int num1,int num2,int num3){

        if(!isValid(num1) || !isValid(num2) || !isValid(num3)) return false;
         ArrayList<Integer> lastDigits = new ArrayList<>();
         lastDigits.add(num1%10);
         lastDigits.add(num2%10);
         lastDigits.add(num3%10);

         int counter =0;

        for(int i = 0; i < lastDigits.size();i++){
            for(int j = 0; j < lastDigits.size(); j++){
                if(lastDigits.get(i).equals(lastDigits.get(j))) counter++;
            }
        }

        return counter >= 2;

    }
}
