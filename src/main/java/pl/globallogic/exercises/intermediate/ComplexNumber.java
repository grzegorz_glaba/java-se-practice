package pl.globallogic.exercises.intermediate;

public class ComplexNumber {
    public double getReal() {
        return real;
    }

    public double getImaginary() {
        return imaginary;
    }

    double real;
    double imaginary;

    public ComplexNumber(double real,double imaginary){
        this.imaginary = imaginary;
        this.real = real;
    }
    public void add(double real,double imaginary){
        this.real += real;
        this.imaginary += imaginary;
    }

    public void add(ComplexNumber complexNumber){
        this.imaginary += complexNumber.getImaginary();
        this.real += complexNumber.getReal();
    }

    public void subtract(double real,double imaginary){
        this.real -= real;
        this.imaginary -= imaginary;
    }

    public void subtract(ComplexNumber complexNumber){
        this.imaginary -= complexNumber.getImaginary();
        this.real -= complexNumber.getReal();
    }

}
