package pl.globallogic.exercises.intermediate;

import java.util.HashMap;
import java.util.Map;

public class Ex1NumberInWord {

    public static void printNumberInWord(int number){
        Map<Integer,String> numbersDictionary = new HashMap<>();
        numbersDictionary.put(0,"ZERO");
        numbersDictionary.put(1,"ONE");
        numbersDictionary.put(2,"TWO");
        numbersDictionary.put(3,"THREE");
        numbersDictionary.put(4,"FOUR");
        numbersDictionary.put(5,"FIVE");
        numbersDictionary.put(6,"SIX");
        numbersDictionary.put(7,"SEVEN");
        numbersDictionary.put(8,"EIGHT");
        numbersDictionary.put(9,"NINE");

        if(!numbersDictionary.containsKey(number)){
            System.out.println("OTHER");
        } else {
            System.out.println(numbersDictionary.get(number));
        }
    }
    public static void main(String[] args) {
        printNumberInWord(8);
        printNumberInWord(20);
        printNumberInWord(-50);
        printNumberInWord(1);

    }
}
