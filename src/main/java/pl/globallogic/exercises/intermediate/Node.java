package pl.globallogic.exercises.intermediate;

public class Node extends ListItem{
    public Node(Object value){
        super(value);
    }

    @Override
    ListItem next() {
        return this.rightLink;
    }

    @Override
    ListItem setNext(ListItem next) {
        this.rightLink = next;
        return rightLink;
    }

    @Override
    ListItem setPrevious(ListItem previous) {
        this.leftLink = previous;
        return leftLink;
    }

    @Override
    int compareTo(ListItem item) {
        if(item != null){
            return ((Comparable<Object>) this.value).compareTo(item.getValue());
        }
        else{
            return -1;
        }
    }

    @Override
    ListItem previous() {
        return this.leftLink;
    }


}

