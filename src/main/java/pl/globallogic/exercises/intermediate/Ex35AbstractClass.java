package pl.globallogic.exercises.intermediate;

public class Ex35AbstractClass {
    public static void main(String[] args) {
        ListItem root = new Node("5");
        MyLinkedList list = new MyLinkedList(root);

        list.addItem(new Node("3"));
        list.addItem(new Node("7"));
        list.addItem(new Node("2"));
        list.addItem(new Node("4"));

        System.out.println("Linked List:");
        list.traverse(list.getRoot());

        list.removeItem(new Node("3"));
        list.removeItem(new Node("6")); // Trying to remove a non-existent item

        System.out.println("Linked List after removal:");
        list.traverse(list.getRoot());

        // Testing SearchTree
        ListItem treeRoot = new Node("50");
        SearchTree tree = new SearchTree(treeRoot);

        tree.addItem(new Node("30"));
        tree.addItem(new Node("70"));
        tree.addItem(new Node("20"));
        tree.addItem(new Node("40"));
        tree.addItem(new Node("60"));
        tree.addItem(new Node("80"));

        System.out.println("\nBinary Search Tree (Inorder Traversal):");
        tree.traverse(tree.getRoot());

        tree.removeItem(new Node("30"));
        tree.removeItem(new Node("75")); // Trying to remove a non-existent item

        System.out.println("\nBinary Search Tree after removal (Inorder Traversal):");
        tree.traverse(tree.getRoot());
    }
}
