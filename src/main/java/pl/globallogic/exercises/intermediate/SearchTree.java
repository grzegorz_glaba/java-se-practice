package pl.globallogic.exercises.intermediate;

public class SearchTree implements NodeList {
    private ListItem root;

    public SearchTree(ListItem root) {
        this.root = root;
    }

    @Override
    public ListItem getRoot() {
        return root;
    }

    @Override
    public boolean addItem(ListItem newItem) {
        if (newItem == null) {
            return false;
        }

        if (root == null) {
            root = newItem;
            return true;
        }

        ListItem currentItem = root;
        while (true) {
            int comparison = currentItem.compareTo(newItem);
            if (comparison < 0) {
                if (currentItem.next() != null) {
                    currentItem = currentItem.next();
                } else {
                    currentItem.setNext(newItem);
                    return true;
                }
            } else if (comparison > 0) {
                if (currentItem.previous() != null) {
                    currentItem = currentItem.previous();
                } else {
                    currentItem.setPrevious(newItem);
                    return true;
                }
            } else {
                return false; // Item already exists
            }
        }
    }

    @Override
    public boolean removeItem(ListItem item) {
        if (item != null) {
            if (root == null) {
                return false; // Tree is empty
            }

            return performRemoval(root, item);
        }
        return false;
    }

    private boolean performRemoval(ListItem currentItem, ListItem itemToRemove) {
        if (currentItem == null) {
            return false; // Item not found
        }

        int comparison = currentItem.compareTo(itemToRemove);
        if (comparison == 0) {
            performRemoval(currentItem, currentItem.previous());
            performRemoval(currentItem, currentItem.next());
            return true;
        } else if (comparison < 0) {
            return performRemoval(currentItem.next(), itemToRemove);
        } else {
            return performRemoval(currentItem.previous(), itemToRemove);
        }
    }

    @Override
    public void traverse(ListItem root) {
        if (root != null) {
            traverse(root.previous());
            System.out.println(root.getValue());
            traverse(root.next());
        }
    }
}
