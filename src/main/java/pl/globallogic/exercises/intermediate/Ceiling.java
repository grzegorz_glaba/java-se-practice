package pl.globallogic.exercises.intermediate;

public class Ceiling {
    private int height;

    public int getHeight() {
        return height;
    }

    public int getPaintedColor() {
        return paintedColor;
    }

    private int paintedColor;


    public Ceiling(int height, int paintedColor) {
        this.height = height;
        this.paintedColor = paintedColor;
    }
}
