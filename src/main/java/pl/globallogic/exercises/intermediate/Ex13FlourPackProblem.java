package pl.globallogic.exercises.intermediate;

public class Ex13FlourPackProblem {
    public static void main(String[] args) {
        System.out.println(canPack(1, 0, 4));
        System.out.println(canPack(1, 0, 5));
        System.out.println(canPack(0, 5, 4));
        System.out.println(canPack(2, 2, 11));
        System.out.println(canPack(-3, 2, 12));


    }

    private static boolean canPack(int bigCount, int smallCount, int goal) {
        if (bigCount < 0 || smallCount < 0 || goal < 0) return false;

        int currentKilos = bigCount * 5 + smallCount;
        int temp = 0;
        if (currentKilos >= goal) {
            while (temp < goal) {
                if (bigCount > 0 && temp + 5 <= goal) {
                    temp += 5;
                    bigCount--;
                } else if (smallCount > 0 && temp + 1 <= goal) {
                    temp += 1;
                    smallCount--;
                } else {
                    break;
                }
            }
            if (temp == goal) return true;


        }
        return false;
    }
}
