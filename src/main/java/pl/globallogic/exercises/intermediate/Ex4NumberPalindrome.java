package pl.globallogic.exercises.intermediate;
public class Ex4NumberPalindrome {


    public static void main(String[] args) {
       System.out.println(isPalindrome(-1221));
        System.out.println(isPalindrome(707));
        System.out.println(isPalindrome(11212));
    }

    private static boolean isPalindrome(Integer number) {
        Integer reverseNumber = 0;
        Integer numberCopy = number;
        for(;numberCopy !=0;numberCopy /= 10){
            Integer remainder = numberCopy%10;
            reverseNumber = reverseNumber * 10 + remainder;
        }
        return reverseNumber.equals(number);
    }
}
