package pl.globallogic.exercises.intermediate;

import java.util.ArrayList;
import java.util.Scanner;

public class Ex15InputCalculator {
    public static void main(String[] args) {
        inputThenPrintSumAndAverage();
    }

    private static void inputThenPrintSumAndAverage(){
        Scanner scanner = new Scanner(System.in);
        ArrayList<Integer> numbers = new ArrayList<>();
        System.out.println("Provide input");
        while(scanner.hasNextInt()){
            Integer temp = scanner.nextInt();
            numbers.add(temp);
        }
        long avg = 0;
        int sum = 0;
        if(!numbers.isEmpty()){
            for(int i = 0;i < numbers.size();i++){
                sum += numbers.get(i);
            }
            avg = sum/numbers.size();
        }

        System.out.println("SUM = " + sum + " AVG = " + avg);

    }
}
