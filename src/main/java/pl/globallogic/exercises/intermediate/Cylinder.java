package pl.globallogic.exercises.intermediate;

public class Cylinder extends Circle {
    public double getHeight() {
        return height;
    }

    double height;

    public Cylinder(double radius,double height){
        super(radius);
        if(height < 0){
            this.height = 0;
        }else this.height = height;
    }

    public double getVolume(){
        return this.getArea()*height;
    }

}
