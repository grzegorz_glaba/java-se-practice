package pl.globallogic.exercises.intermediate;

public class Contact {
    String name;

    public String getName() {
        return name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    String phoneNumber;

    public Contact(String name,String phoneNumber){
        this.name = name;
        this.phoneNumber = phoneNumber;
    }

    public static Contact createContact(String name,String phoneNumber){
        return new Contact(name,phoneNumber);
    }
}
