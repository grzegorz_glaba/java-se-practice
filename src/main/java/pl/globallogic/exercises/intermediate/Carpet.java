package pl.globallogic.exercises.intermediate;

public class Carpet {
    public double getCost() {
        return cost;
    }
    double cost;
    public Carpet(double cost){
        if(cost < 0){
            this.cost = 0;
        }else{
            this.cost = cost;
        }
    }
}
