package pl.globallogic.exercises.intermediate;

public abstract class ListItem {
    protected ListItem rightLink;
    protected ListItem leftLink;
    protected Object value;

    public ListItem(Object value){
        this.value = value;
    }

    abstract ListItem next();
    abstract ListItem setNext(ListItem next);
    abstract ListItem setPrevious(ListItem previous);
    abstract int compareTo(ListItem item);
    abstract ListItem previous();

    public Object getValue(){
        return this.value;
    }
    public void setValue(Object value){
        this.value = value;
    }
}
