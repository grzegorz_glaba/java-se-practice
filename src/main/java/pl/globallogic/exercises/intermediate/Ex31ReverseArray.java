package pl.globallogic.exercises.intermediate;

public class Ex31ReverseArray {

    private static void printArray(int[] numbersArray){
        System.out.print("Array = [");
        for(int i = 0;i<numbersArray.length-1;i++){
            System.out.print(numbersArray[i] + ", ");
        }
        System.out.print(numbersArray[numbersArray.length-1] + "]\n");

    }
    private static void reverse(int[] numbersArray){
        int arraySize = numbersArray.length - 1;
        int lastIndex = numbersArray[arraySize];
        for(int i = 0;i<arraySize;i++) {
            int temp = numbersArray[i];
            numbersArray[i] = numbersArray[arraySize];
            numbersArray[arraySize] = temp;
            --arraySize;
        }
    }
    public static void main(String[] args) {
        int[] numbersArray = {1,2,3,4,5};
        printArray(numbersArray);
        reverse(numbersArray);
        printArray(numbersArray);


    }
}
