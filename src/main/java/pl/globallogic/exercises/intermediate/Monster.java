package pl.globallogic.exercises.intermediate;

import java.util.ArrayList;
import java.util.List;

public class Monster implements ISaveable {
    String name;
    int hitPoints;
    int strength;

    public Monster(String name,int hitPoints,int strength){
        this.name = name;
        this.hitPoints = hitPoints;
        this.strength = strength;
    }

    public String getName() {
        return name;
    }

    public int getHitPoints() {
        return hitPoints;
    }

    public int getStrength() {
        return strength;
    }
    public List<String> write(){
        List<String> fields = new ArrayList<String>();
        fields.add(this.name);
        fields.add(String.valueOf(this.hitPoints));
        fields.add(String.valueOf(this.strength));
        return fields;
    }
    public void read(List<String> list){
        if (list != null && list.size() == 3) {
            this.name = list.get(0);
            this.hitPoints = Integer.parseInt(list.get(1));
            this.strength = Integer.parseInt(list.get(2));
        } else {
            System.out.println("Invalid saved values.");
        }
    }

    public String toString(){
        return "Monster{name="+this.name+", hitpoints=" + this.hitPoints +", strength=" + this.strength +"}";
    }

}
