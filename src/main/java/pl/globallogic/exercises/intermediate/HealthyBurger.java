package pl.globallogic.exercises.intermediate;

public class HealthyBurger extends Hamburger{
    String healthyExtra1Name;
    Double healthyExtra1Price = 0.0;
    String healthyExtra2Name;
    Double healthyExtra2Price = 0.0;

    public HealthyBurger(String meat, double price) {
        super("Healthy burger", meat, price, "Brown rye roll");
    }

    public void addHealthyAddition1(String healthyExtra1Name,Double healthyExtra1Price){
        this.healthyExtra1Name = healthyExtra1Name;
        this.healthyExtra1Price = healthyExtra1Price;
        System.out.println("Added " + healthyExtra1Name + " for  " + healthyExtra1Price);

    }
    public void addHealthyAddition2(String healthyExtra2Name,Double healthyExtra2Price){
        this.healthyExtra2Name = healthyExtra2Name;
        this.healthyExtra2Price = healthyExtra2Price;
        System.out.println("Added " + healthyExtra2Name + " for  " + healthyExtra2Price);
    }

    @Override
    public double itemizeHamburger() {
        return super.itemizeHamburger() + this.healthyExtra2Price + this.healthyExtra1Price;
    }
}
