package pl.globallogic.exercises.intermediate;

public class Ex2NumbersOfDayInMonth {

    public static boolean isLeapYear(int year) {
        if (year < 1 || year > 9999) {
            System.out.println("Invalid parameter!");
            return false;
        }
        if (year % 4 == 0) {
            if(year%100 == 0){
                return year % 400 == 0;
            }
            else{
                return true;
            }
        }
        return false;
    }

    public static int getDaysInMonth(int month,int year){
        if(month < 1 || month > 12 || year<1 || year>9999){
            return -1;
        }
        else if(month == 2){
            if(isLeapYear(year)) {
                return 29;
            }else{
                return 28;
            }
        }
        else if(month <= 7){
            return 30 + month%2;
        }else{
            return 30 + (month+1)%2;
        }
    }
    public static void main(String[] args) {
        System.out.println(getDaysInMonth(1,2020));
        System.out.println(getDaysInMonth(2,2020));
        System.out.println(getDaysInMonth(2,2018));
        System.out.println(getDaysInMonth(3,1919191));
        System.out.println(getDaysInMonth(7,2001));


    }
}
