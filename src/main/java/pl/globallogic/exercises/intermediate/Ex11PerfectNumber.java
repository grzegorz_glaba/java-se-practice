package pl.globallogic.exercises.intermediate;

import java.util.ArrayList;

public class Ex11PerfectNumber {
    public static void main(String[] args) {
        System.out.println(isPerfectNumber(6) == true);
        System.out.println(isPerfectNumber(28) == true);
        System.out.println(isPerfectNumber(5) == false);
        System.out.println(isPerfectNumber(-1) == false);

    }

    private static boolean isPerfectNumber(int number) {

        if(number<1) return false;

        ArrayList<Integer> divisors = new ArrayList<>();
        Integer sum = 0;

        for(int i = 1; i < number; i++){
            if(number%i == 0){
                divisors.add(i);
            }
        }
        for(int j : divisors){
            sum += j;
        }

        return sum.equals(number);
    }


}
