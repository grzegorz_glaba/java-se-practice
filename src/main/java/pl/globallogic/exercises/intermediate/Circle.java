package pl.globallogic.exercises.intermediate;

public class Circle {
    public double getRadius() {
        return radius;
    }

    double radius;

    public Circle(double radius){
        if(radius<0){
            this.radius = 0;
        }else this.radius = radius;
    }

    public double getArea(){
        return radius*radius*Math.PI;
    }


}
