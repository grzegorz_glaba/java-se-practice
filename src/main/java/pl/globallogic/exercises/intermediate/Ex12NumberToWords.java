package pl.globallogic.exercises.intermediate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Ex12NumberToWords {
    public static void main(String[] args) {
        numberToWords(123);
        numberToWords(1010);
        numberToWords(1000);
        numberToWords(-12);

//        System.out.println(reverse(-121));
//        System.out.println(reverse(1212));
//        System.out.println(reverse(1234));
//        System.out.println(reverse(100));

//        System.out.println(getDigitCount(0));
//        System.out.println(getDigitCount(123));
//        System.out.println(getDigitCount(-12));
//        System.out.println(getDigitCount(5200));


    }

    private static void numberToWords(int number) {
        int reversed = reverse(number);
        int digitsDiff = getDigitCount(number) - getDigitCount(reversed);

        if(number < 0) System.out.println("Invalid value");

        Map<Integer,String> numbersDictionary = new HashMap<>();
        numbersDictionary.put(0,"ZERO");
        numbersDictionary.put(1,"ONE");
        numbersDictionary.put(2,"TWO");
        numbersDictionary.put(3,"THREE");
        numbersDictionary.put(4,"FOUR");
        numbersDictionary.put(5,"FIVE");
        numbersDictionary.put(6,"SIX");
        numbersDictionary.put(7,"SEVEN");
        numbersDictionary.put(8,"EIGHT");
        numbersDictionary.put(9,"NINE");

        while(reversed>0){
            System.out.print(numbersDictionary.get(reversed%10) + " " );
            reversed /= 10;
        }
        if(digitsDiff > 0){
            for(int i = 0; i < digitsDiff; i++){
                System.out.print(" " + numbersDictionary.get(0) + " ");
            }
        }
        System.out.println(" ");
        System.out.println(" ");
    }

    private static int getDigitCount(int number) {
        if(number<0) return -1;
        if(number == 0) return 1;

        int digitCounter = 0;

        while(number > 0){
            number /= 10;
            digitCounter++;
        }
        return digitCounter;
    }

    private static int reverse(int number) {
        boolean isNegative = false;
        if(number<0){
            isNegative = true;
            number *= -1;
        }
        ArrayList<Integer> digits = new ArrayList<>();
        int temp = number;
        while(temp > 0){
            digits.add(temp%10);
            temp /= 10;
        }
        //printArray(digits);
        int digitsArrSize = digits.size() -1;

        for(int i = 0; i < digitsArrSize; i++){
            digits.add(i,digits.remove(digitsArrSize));
        }
        //printArray(digits);
        int reversedNumber = 0;
        for(int j = 0; j < digitsArrSize + 1; j++){
            reversedNumber = reversedNumber*10 + digits.get(digitsArrSize - j);
        }

        if(isNegative) return reversedNumber*(-1);

        return reversedNumber;

    }

    private static void printArray(ArrayList<Integer> list){
        for(int i : list){
            System.out.print(i + " ");
        }
    }
}
