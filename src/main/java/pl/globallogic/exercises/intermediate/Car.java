package pl.globallogic.exercises.intermediate;

public class Car {
    private boolean engine = false;

    public int getCylinders() {
        return cylinders;
    }

    public String getName() {
        return name;
    }

    private int cylinders;
    private String name;
    private int wheels = 4;


    public Car(int cylinders, String name) {
        this.cylinders = cylinders;
        this.name = name;
    }

    public String startEngine(){
        this.engine = true;
        return (this.name + "'s engine is starting");
    }
    public String accelerate(){
        return (this.name + " is accelerating");
    }
    public String brake(){
        return (this.name + " is braking");
    }
}
