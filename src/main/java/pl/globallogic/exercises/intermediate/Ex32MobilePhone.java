package pl.globallogic.exercises.intermediate;

public class Ex32MobilePhone {
    public static void main(String[] args) {
        MobilePhone mobilePhone = new MobilePhone("1234567890");
        Contact contact1 = Contact.createContact("Grzesiek", "31415926");
        Contact contact2 = Contact.createContact("Marta", "16180339");
        Contact contact3 = Contact.createContact("Adam", "11235813");
        Contact contact4 = Contact.createContact("Mikolaj", "23571113");

        mobilePhone.addNewContact(contact1);
        mobilePhone.addNewContact(contact2);
        mobilePhone.addNewContact(contact3);
        mobilePhone.addNewContact(contact4);

        mobilePhone.printContacts();

        Contact newContact = Contact.createContact("Janusz", "98765432");
        mobilePhone.updateContact(contact1, newContact);
        mobilePhone.printContacts();

        mobilePhone.removeContact(contact2);
        mobilePhone.printContacts();

        Contact queriedContact = mobilePhone.queryContact("Jarek");
        if (queriedContact != null) {
            System.out.println("Queried contact: " + queriedContact.getName() + " -> " + queriedContact.getPhoneNumber());
        } else {
            System.out.println("Contact not found.");
        }
    }
}
