package pl.globallogic.exercises.intermediate;

import java.util.ArrayList;

public class Bank {
    private String name;
    private ArrayList<Branch> branches;

    public Bank(String nameOfTheBank){
        this.name = nameOfTheBank;
        this.branches = new ArrayList<Branch>();
    }
    public boolean addBranch(String nameOfTheBranch){
        if(findBranch(nameOfTheBranch)==null){
            this.branches.add(new Branch(nameOfTheBranch));
            return true;
        }
        return false;
    }

    public boolean addCustomer(String nameOfTheBranch,String nameOfTheCustomer,double transaction){
        Branch tempBranch = findBranch(nameOfTheBranch);
        if(tempBranch != null){
            tempBranch.newCustomer(nameOfTheCustomer,transaction);
            return true;
        }
        return false;
    }

    public boolean addCustomerTransaction(String nameOfTheBranch,String nameOfTheCustomer,double transaction){
        Branch tempBranch = findBranch(nameOfTheBranch);
        if(tempBranch != null){
            tempBranch.addCustomerTransaction(nameOfTheCustomer,transaction);
            return true;
        }
        return false;
    }

    public Branch findBranch(String nameOfTheBranch){
        for(Branch branch : branches ){
            if(branch.getName().equals(nameOfTheBranch)){
                return branch;
            }
        }
        return null;
    }

    public boolean listCustomers(String nameOfTheBranch,boolean printTransactions){
        if(findBranch(nameOfTheBranch) != null){
            if(printTransactions){
                ArrayList<Customer> tempBranchCustomers = findBranch(nameOfTheBranch).getCustomers();
                System.out.println("Customer details for branch " + nameOfTheBranch);
                for(Customer customer : tempBranchCustomers){
                    System.out.println("Customer: " + customer.getName());
                    System.out.println("Transactions");
                    for(int i = 0; i < customer.getTransactions().size(); i++){
                        System.out.println("[" + i + "]" + " Amount " + customer.getTransactions().get(i));
                    }
                }
                return true;
            }
            else{
                System.out.println("Customer details for branch " + nameOfTheBranch);
                for(Customer customer : findBranch(nameOfTheBranch).getCustomers()){
                    System.out.println("Customer: " + customer.getName());
                }
                return true;
            }
        }
        return false;
    }
}
