package pl.globallogic.exercises.intermediate;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Ex10GreatestCommonDivisor {
    public static void main(String[] args) {
        System.out.println(getGreatestCommonDivisor(25,15) == 5);
        System.out.println(getGreatestCommonDivisor(12,30) == 6);
        System.out.println(getGreatestCommonDivisor(9,18) == -1);
        System.out.println(getGreatestCommonDivisor(81,153) == 9);

    }

    private static int getGreatestCommonDivisor(int first, int second) {
        if(first < 10 || second < 10) return -1;

        int greatestDivisor = 0;

        ArrayList<Integer> duplicates = new ArrayList<>();
        Set<Integer> divisors = new HashSet<>();
        for(int i = 1; i <= first; i++){
            if(first%i == 0) {
                if(divisors.contains(i)){
                    duplicates.add(i);
                }
                else divisors.add(i);
            }
        }
        for(int j = 1; j <= second; j++){
            if(second % j == 0) {
                if(divisors.contains(j)){
                    duplicates.add(j);
                }
                else divisors.add(j);
            }
        }

        for(Integer i : duplicates){
            if(greatestDivisor < i) greatestDivisor = i;
        }
        return greatestDivisor;

    }
}
