package pl.globallogic.exercises.intermediate;

import java.util.ArrayList;

public class Ex14LargestPrime {
    public static void main(String[] args) {
        System.out.println(getLargestPrime(21) == 7);
        System.out.println(getLargestPrime(217) == 31);
        System.out.println(getLargestPrime(0) == -1);
        System.out.println(getLargestPrime(5) == 5);
        System.out.println(getLargestPrime(-1) == -1);

    }

    private static int getLargestPrime(int number) {
        if(number <= 1) return -1;
        boolean[] tempTab = new boolean[number+1];
        int biggestPrime = 0;

        for(int i = 2; i <= number ; i++){
            tempTab[i] = true;
        }
        for(int i = 2; i <= Math.sqrt(number); i++){
            if(tempTab[i] == true){
                for(int k = i*i; k <= number; k+=i){
                    tempTab[k] = false;
                }
            }
        }

        for(int i = 2; i <= number; i++){
            if(tempTab[i] == true && number % i == 0){
                if(i > biggestPrime){
                    biggestPrime = i;
                }
            }
        }

        return biggestPrime;

    }


}
