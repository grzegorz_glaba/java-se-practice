package pl.globallogic.exercises.intermediate;

import java.util.ArrayList;

public class Customer {
    public String getName() {
        return name;
    }

    public ArrayList<Double> getTransactions() {
        return transactions;
    }

    private String name;
    private ArrayList<Double> transactions;

    public Customer(String nameOfTheCustomer,double initialTransaction){
        this.name = nameOfTheCustomer;
        this.transactions = new ArrayList<Double>();
        transactions.add(initialTransaction);
    }

    public void addTransaction(double transaction){
        transactions.add(transaction);
    }
}
