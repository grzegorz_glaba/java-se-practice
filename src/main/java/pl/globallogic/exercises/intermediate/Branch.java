package pl.globallogic.exercises.intermediate;

import java.util.ArrayList;

public class Branch {
    private String name;

    public String getName() {
        return name;
    }

    public ArrayList<Customer> getCustomers() {
        return customers;
    }

    private ArrayList<Customer> customers;

    public Branch(String branch){
        this.name = branch;
        this.customers = new ArrayList<Customer>();
    }
    public boolean newCustomer(String nameOfCustomer,double initialTransaction){
        if(findCustomer(nameOfCustomer) == null){
            Customer newCustomer = new Customer(nameOfCustomer,initialTransaction);
            this.customers.add(newCustomer);
            return true;
        }
        return false;
    }

    public boolean addCustomerTransaction(String name,double transaction){
        if(findCustomer(name) != null){
            findCustomer(name).addTransaction(transaction);
            return true;
        }
        return false;
    }

    public Customer findCustomer(String name){
        for(Customer customer : customers){
            if(customer.getName().equals(name)){
                return customer;
            }
        }
        return null;
    }

}
