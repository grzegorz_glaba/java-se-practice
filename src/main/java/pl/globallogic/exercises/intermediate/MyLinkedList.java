package pl.globallogic.exercises.intermediate;


public class MyLinkedList implements NodeList {

    private ListItem root;

    public MyLinkedList(ListItem root) {
        this.root = root;
    }

    public ListItem getRoot() {
        return this.root;
    }

    public boolean addItem(ListItem item) {
        if (root == null) {
            // The list is empty, so set the root to the new item
            root = item;
            return true;
        }

        ListItem currentItem = root;
        while (currentItem != null) {
            int comparison = currentItem.compareTo(item);
            if (comparison == 0) {
                return false;
            } else if (comparison < 0) {
                if (currentItem.next() != null) {
                    currentItem = currentItem.next();
                } else {
                    currentItem.setNext(item);
                    item.setPrevious(currentItem);
                    return true;
                }
            } else {
                ListItem previousItem = currentItem.previous();
                item.setNext(currentItem);
                item.setPrevious(previousItem);
                currentItem.setPrevious(item);

                if (previousItem != null) {
                    previousItem.setNext(item);
                } else {
                    root = item;
                }

                return true;
            }
        }
        return false;
    }


    public boolean removeItem(ListItem item) {
        if (root == null) {
            // The list is empty, nothing to remove
            return false;
        }

        ListItem currentItem = root;
        while (currentItem != null) {
            int comparison = currentItem.compareTo(item);
            if (comparison == 0) {
                // Found the item to remove
                ListItem previousItem = currentItem.previous();
                ListItem nextItem = currentItem.next();

                if (previousItem != null) {
                    previousItem.setNext(nextItem);
                } else {
                    // Current item is the root, update the root
                    root = nextItem;
                }

                if (nextItem != null) {
                    nextItem.setPrevious(previousItem);
                }

                return true;
            } else if (comparison < 0) {
                currentItem = currentItem.next();
            } else {
                // The item to remove is not in the list
                return false;
            }
        }
        return false;
    }

    public void traverse(ListItem root) {
        if (root == null) {
            System.out.println("The list is empty.");
        } else {
            ListItem currentItem = root;
            while (currentItem != null) {
                System.out.println(currentItem.getValue());
                currentItem = currentItem.next();
            }
        }
    }

}
