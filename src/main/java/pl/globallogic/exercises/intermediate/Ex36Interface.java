package pl.globallogic.exercises.intermediate;

import java.util.List;

public class Ex36Interface {
    public static void main(String[] args) {
        Player player = new Player("Tim", 10, 15);
        Monster monster = new Monster("Werewolf", 20, 40);

        List<String> playerData = player.write();
        List<String> monsterData = monster.write();

        Player loadedPlayer = new Player("", 0, 0);
        Monster loadedMonster = new Monster("", 0, 0);

        loadedPlayer.read(playerData);
        loadedMonster.read(monsterData);

        System.out.println("Loaded Player: " + loadedPlayer);
        System.out.println("Loaded Monster: " + loadedMonster);
    }
}
