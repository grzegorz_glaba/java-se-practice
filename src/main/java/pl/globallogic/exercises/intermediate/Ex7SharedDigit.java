package pl.globallogic.exercises.intermediate;

import java.util.ArrayList;

public class Ex7SharedDigit {
    public static void main(String[] args) {
        System.out.println(hasSharedDigit(12,23) == true);
        System.out.println(hasSharedDigit(9,99) == false);
        System.out.println(hasSharedDigit(15,55) == true);

    }

    private static boolean hasSharedDigit(int num1, int num2) {
        if(num1 < 10 || num1 > 99 || num2 < 10 || num2 > 99) return false;


        ArrayList<Integer> firstArray = new ArrayList<>();
        ArrayList<Integer> secondArray = new ArrayList<>();
        while(num1>0){
            firstArray.add(num1%10);
            num1 /= 10;
            secondArray.add(num2%10);
            num2 /= 10;
        }

        for(int j = 0; j < firstArray.size(); j++ ){
            for(int k = 0; k < secondArray.size();k++){
                if(firstArray.get(j) == secondArray.get(k)) return true;
            }
        }
        return false;



    }


}
