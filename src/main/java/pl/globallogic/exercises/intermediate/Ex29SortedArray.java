package pl.globallogic.exercises.intermediate;

import java.util.ArrayList;
import java.util.Scanner;

public class Ex29SortedArray {



    public static void printArray(int[] array){
        for(int i = 0;i < array.length;i++){
            System.out.println("Element " + i + " contents " + array[i]);
        }
    }

    public static int[] getIntegers(int arraySize){
        int[] numbersArray = new int[arraySize];
        Scanner scanner = new Scanner(System.in);
        for(int i = 0;i < arraySize; i++){
            System.out.println("Wprowadz " + i + " element");
            numbersArray[i] = scanner.nextInt();
        }
        return numbersArray;
    }

    public static int[] sortIntegers(int[] numbersArray){

        int size = numbersArray.length;
        for(int i = 1; i < size;i++){
            int sortedNumber = numbersArray[i];
            int j = i - 1;
            while(j>=0 && sortedNumber > numbersArray[j]){
                numbersArray[j+1] = numbersArray[j];
                --j;
            }
            numbersArray[j+1] = sortedNumber;
        }
        return numbersArray;
    }
    public static void main(String[] args) {
    int[] numbersArray = getIntegers(5);
    System.out.println("Przed sortowaniem");
    printArray(numbersArray);
    System.out.println("Po sortowaniu");
    printArray(sortIntegers(numbersArray));


    }

}
