package pl.globallogic.exercises.intermediate;

public class Ex6EvenDigitSum {
    public static void main(String[] args) {
        System.out.println(getEvenDigitSum(123456789) == 20);
        System.out.println(getEvenDigitSum(252) == 4);
        System.out.println(getEvenDigitSum(-252) == -1);

    }

    private static int getEvenDigitSum(int number){
        if(number < 0) return -1;

        int singleDigit = 0;
        int sum = 0;

        while(number>0){
            singleDigit = number%10;
            if(singleDigit%2 == 0) sum += singleDigit;
            number /= 10;
        }

        return sum;
    }
}
