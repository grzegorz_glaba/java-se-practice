package pl.globallogic.exercises.intermediate;

public class DeluxeBurger extends Hamburger {


    public DeluxeBurger(){
        super("Deluxe burger","Lamb meat",14.00,"Whole oats roll");
        this.addition1Name = "Chips";
        this.addition1Price = 3.00;
        this.addition2Price = 2.10;
        this.addition2Name = "Drink";
    };

    @Override
    public void addHamburgerAddition1(String addition1Name, double addition1Price) {
        System.out.println("Can't add any additions");
    }

    @Override
    public void addHamburgerAddition2(String addition2Name, double addition2Price) {
        System.out.println("Can't add any additions");
    }

    @Override
    public void addHamburgerAddition3(String addition3Name, double addition3Price) {
        System.out.println("Can't add any additions");
    }

    @Override
    public void addHamburgerAddition4(String addition4Name, double addition4Price) {
        System.out.println("Can't add any additions");
    }
}
