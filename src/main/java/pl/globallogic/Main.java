package pl.globallogic;

public class Main {
    public static void main(String[] args) {
        int[] input ={1,3,4,2,5,3,2,10,5,11};
        findNextGreaterElementFor(input);

    }

    private static void findNextGreaterElementFor(int[] input){
        int nge;
        for(int i = 0;i <input.length;i++){
            nge = -1;
            for(int j = i+1;j<input.length;j++){
                if(input[i]<input[j]){
                    nge = input[j];
                    break;
                }
                System.out.printf("For %s next greater is %s \n",input[i],nge);
            }
        }

    }
}