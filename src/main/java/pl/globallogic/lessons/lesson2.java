package pl.globallogic.lessons;

import java.util.Scanner;

public class lesson2 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Whats your name?");
        String name = scanner.next();
        //String greeting = "";
//        if(name.equals("Maja")) {
//            greeting = "Cześć " + name;
//        }else if(name.equals("Chen")){
//            greeting = "Nihao " + name;
//
//        }else{
//            greeting = "Hello " + name;
//        }
        System.out.println("How many times?");
        int numberOfGreets = scanner.nextInt();


        String greeting = switch(name.trim()){
            case "Chen"  -> "Nihao " + name;
            case "Maja" -> "Czesc " + name;
            default -> "Hello " + name;
        };
        //System.out.println(greeting);
        for(int i = 0; i < numberOfGreets;i++){
            System.out.println(greeting);
        }

    }
}
